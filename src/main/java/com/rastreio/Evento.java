package com.rastreio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "Evento")
public class Evento {
    @Id
    @GeneratedValue
    private Long id;

    private String descricao;

    private EntidadeParaEvento entidadeTipo;

    private Long entidadeId;

    public Long getId() {
        return id;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setEntidadeTipo(EntidadeParaEvento klass) {
        this.entidadeTipo = klass;
    }

    public EntidadeParaEvento getEntidadeTipo() {
        return entidadeTipo;
    }

    public void setEntidadeId(Long entidadeId) {
        this.entidadeId = entidadeId;
    }

    public Long getEntidadeId() {
        return entidadeId;
    }
}
