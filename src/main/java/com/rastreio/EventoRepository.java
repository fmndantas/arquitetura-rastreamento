package com.rastreio;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EventoRepository extends JpaRepository<Evento, Long> {
    List<Evento> findEventoByEntidadeTipoAndEntidadeId(EntidadeParaEvento tipo, Long id);

    Page<Evento> findEventoByEntidadeTipoAndEntidadeId(EntidadeParaEvento tipo, Long id, Pageable page);

}
