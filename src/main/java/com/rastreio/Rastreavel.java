package com.rastreio;

import javax.persistence.*;

@MappedSuperclass
public abstract class Rastreavel {
    private EntidadeParaEvento entidadeTipo;

    @Id
    @GeneratedValue
    protected Long id;

    Rastreavel(EntidadeParaEvento entidadeTipo) {
        this.entidadeTipo = entidadeTipo;
    }

    public void addEvento(Evento evento) {
        evento.setEntidadeTipo(this.entidadeTipo);
        evento.setEntidadeId(this.id);
    }

    public Long getId() {
        return id;
    }

    public EntidadeParaEvento getEntidadeTipo() {
        return entidadeTipo;
    }
}
