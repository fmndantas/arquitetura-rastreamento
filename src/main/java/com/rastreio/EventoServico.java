package com.rastreio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventoServico {
    private EventoRepository eventoRepository;

    @Autowired
    public EventoServico(EventoRepository eventoRepository) {
        this.eventoRepository = eventoRepository;
    }

    public List<Evento> findAll() {
        return eventoRepository.findAll();
    }

    public Page<Evento> findAllPaged(Pageable pageable) {
        return eventoRepository.findAll(pageable);
    }
}
