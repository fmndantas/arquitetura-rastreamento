package com.rastreio;

import org.springframework.data.jpa.repository.JpaRepository;

public abstract class ServicoRastreador<T extends Rastreavel> {
    protected EventoRepository eventoRepository;

    ServicoRastreador(EventoRepository eventoRepository) {
        this.eventoRepository = eventoRepository;
    }

    public abstract JpaRepository<T, Long> obterRepositorioComum();

    protected T salvar(T entidade) {
        T entidadeSalva = obterRepositorioComum().save(entidade);
        var evento = new Evento();
        entidadeSalva.addEvento(evento);
        evento.setDescricao("Entidade " + entidade + " criada");
        eventoRepository.save(evento);
        return entidadeSalva;
    }
}
