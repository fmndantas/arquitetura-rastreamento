package com.rastreio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class ProdutoServico extends ServicoRastreador<Produto> {
    private ProdutoRepository produtoRepository;

    @Autowired
    ProdutoServico(EventoRepository eventoRepository, ProdutoRepository produtoRepository) {
        super(eventoRepository);
        this.produtoRepository = produtoRepository;
    }

    @Override
    public JpaRepository<Produto, Long> obterRepositorioComum() {
        return this.produtoRepository;
    }
}
