package com.rastreio;

import javax.persistence.Entity;

@Entity(name = "Produto")
public class Produto extends Rastreavel {
    public Produto() {
        super(EntidadeParaEvento.PRODUTO);
    }

    @Override
    public String toString() {
        return "Produto";
    }
}
