package com.rastreio;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RastreioApplicationTests {

    private EventoRepository eventoRepository;
    private ProdutoRepository produtoRepository;
    private ProdutoServico produtoServico;

    @Autowired
    RastreioApplicationTests(EventoRepository eventoRepository,
                             ProdutoRepository produtoRepository,
                             ProdutoServico produtoServico) {
        this.eventoRepository = eventoRepository;
        this.produtoRepository = produtoRepository;
        this.produtoServico = produtoServico;
    }

    @BeforeEach
    public void setUp() {
    }

    @Test
    void contextLoads() {
    }

    @Test
    void salvarProduto() {
        produtoServico.salvar(new Produto());
    }

    @Test
    void aoSalvarProdutoEventoDeveSerCriado() {
        var produto = new Produto();
        produtoServico.salvar(produto);
        var eventos = eventoRepository.findAll();
        Assertions.assertEquals(1, eventos.size());
        var evento = eventoRepository.findEventoByEntidadeTipoAndEntidadeId(
                        produto.getEntidadeTipo(),
                        produto.getId()
                )
                .get(0);
        Assertions.assertEquals(produto.getId(), evento.getEntidadeId());
        Assertions.assertEquals(produto.getEntidadeTipo(), evento.getEntidadeTipo());
    }

    @AfterEach
    void tearDown() {
        eventoRepository.deleteAll();
    }
}
